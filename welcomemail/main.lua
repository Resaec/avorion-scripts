package.path = package.path .. ";data/scripts/lib/?.lua"

include ("utility")

-- namespace welcomemail
welcomemail = {}

welcomemail.updateMulti = 1/60
welcomemail.playerGotWelcomeMailFlag = "gotWelcomeMail"

welcomemail.newplayermessage = 
[[Hallo und Willkommen im Wurstiverse!
>English below<

Server und Konfiguration:

Dies ist ein deutscher Community Server, hier wird in erster Linie Deutsch gesprochen. Wir werden aber auch auf andere Sprachen Rücksicht nehmen, sofern es in unserer Macht steht und bitten euch um Verständnis. ;)

Der Server ist bis auf einige Kleinigkeiten ungemoded und soll dies auch bleiben.
Wir möchten mit euch gemeinsam die Entwicklung von Avorion abwarten und hoffen das euch zunächst die vanilla Features ausreichen.

Sollte euer Spielerlebnis in irgendeiner Form negativ vom Server beeinflusst werden, so teilt uns das bitte umgehend mit!

Regeln:

Beleidungungen/Beschimpfung und Griefing sind zu unterlassen.
Es wird nicht gerne gesehen wenn Stationen oder ganze PVE Parteien von der Karte verschwinden.
Seid so fair und raided keine anderen Spieler, während diese offline sind...
Bug-abuse, Glitching und Hacken wird mit Spielpausen bis lebenslang belohnt!
Es ist nicht gestattet Avorion zu verteilen.

Ihr könnt euch die Regeln jederzeit mit /regeln in's Gedächtnis rufen. ;)

Für eure Kommunikation steht ein Discord Server bzw. aber lieber noch ein TS3-Server zur Verfügung. Dieser verfügt leider nur über eine begrenzte Anzahl an Slots. Wir bitten daher um euer Verständnis, sollte er bereits voll sein.

TS3: Duuusssch.de:9987 PW: cool
Discord: https://discord.gg/Ug7Pn26

Wir wünschen euch viel Spaß und hoffen, dass die beigelegten Resourcen für den Wall of Text entschuldigen und hilfreich sind. ;)

Mit freundlichen Grüßen eure Admin's, Absurdon, ForReal-_- und Link.

-----------------------------------------

Hi and welcome to the Wurstiverse!

Server and configuration:

This is a german community server. We will almost exclusively write in german here. Whenever possible or necessary we will try to help you out in english, too. ;)

The server is almost 100% vanilla and we want to keep it that way until the game gets more content.
We hope to see you wait with us ;D

If you experience any problems on our server please tell us right away!

Rules:

Insulting others and griefing will not be tolerated.
Please don't destroying npc stations or entire factions.
Be fair and don't raid offline players.
Bug-abuse, glitching and hacking will be punished with (temporary) bans!
You are not allowed to distribute Avorion.

You can always review these rules by using /rules.

For your communication you might want to use our TS3 or Discord servers. Please keep in mind that the TS3 server has limited slots and thus it is possible that there might not be one left for you some times.

TS3: Duuusssch.de:9987 PW: cool
Discord: https://discord.gg/Ug7Pn26

We hope you have fun and wish you good luck in the cold galaxy of the Wurstiverse. Take these resources for a head start!

Your "trusty" admin's Absurdon, ForReal-_- and Link.]]

welcomemail.rulesMail = Mail();
    welcomemail.rulesMail.sender = "Server Mailbot"
    welcomemail.rulesMail.header = "Willkommensmail / Welcome mail"
    welcomemail.rulesMail.text = welcomemail.newplayermessage
    welcomemail.rulesMail.money = 25000
    welcomemail.rulesMail:setResources(10000,10000,0,0,0,0,0)
    

welcomemail.thanksMail = Mail();
    welcomemail.thanksMail.sender = "Server Mailbot"
    welcomemail.thanksMail.header = "Danke / Thanks"
    welcomemail.thanksMail.text = welcomemail.newplayermessage
    welcomemail.thanksMail.money = 25000
    welcomemail.thanksMail:setResources(10000,10000,0,0,0,0,0)

function welcomemail.getUpdateInterval()

    return 60 * welcomemail.updateMulti
	
end

function welcomemail.initialize()

	print("==> WelcomeMail - loaded")
	
end

function welcomemail.update(timeStep)

	welcomemail.updateMulti = 60
	Galaxy():registerCallback("onPlayerCreated", "handleSendWelcomeMail")
	Server():registerCallback("onPlayerLogIn", "handleReturningPlayer")
	
end

function welcomemail.handleSendWelcomeMail(index)

	local player = Player(index)
	
	welcomemail.sendNewPlayerMail(player)
	player:setValue(welcomemail.playerGotWelcomeMailFlag, true)
	print("=> New player '%s' (%i), welcome mail send", player.name, index)
	
end

function welcomemail.sendNewPlayerMail(player)

    --Sende dem Spieler eine Begrüßungsmail
    player:addMail(welcomemail.rulesMail)
	
end

function welcomemail.handleReturningPlayer(index)
	
	
	local player = Player(index)
	local flag = player:getValue(welcomemail.playerGotWelcomeMailFlag)
	
	--if the player already got the mail, we leave him be
	if flag ~= nil then
		return
	end
	
	welcomemail.sendReturningPlayerMail(player)
	player:setValue(welcomemail.playerGotWelcomeMailFlag, true)
	print("=> Returning player '%s' (%i), thanks mail send", player.name, index)
	
	
end

function welcomemail.sendReturningPlayerMail(player)

    --Sende dem Spieler eine Dankesmail   
    player:addMail(welcomemail.thanksMail)
	
end