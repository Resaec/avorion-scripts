local rulestrings = {}

rulestrings.de = 
[[
Beleidungungen/Beschimpfung und Griefing sind zu unterlassen.
Es wird nicht gerne gesehen wenn Stationen oder ganze PVE Parteien von der Karte verschwinden.
Seid so fair und raided keine anderen Spieler, während diese offline sind...
Buguseing, Glitching und Hacken wird mit (temporären) Bans bestraft!
Es ist nicht gestattet Avorion zu verteilen.
]]

rulestrings.en =
[[
Insulting others and griefing will not be tolerated.
Please don't destroying npc stations or entire factions.
Be fair and don't raid offline players.
Bugusing, glitching and hacking will be punished with (temporary) bans!
You are not allowed to distribute Avorion.
]]

return rulestrings
