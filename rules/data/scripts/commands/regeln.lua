package.path = package.path .. ";data/scripts/lib/?.lua"

-- namespace regeln
regeln = {}
regeln.rulesfile = include("\\data\\libs\\rulestrings")

function regeln.execute(sender, ...)

	if sender == nil then return end
	Player(sender):sendChatMessage("", ChatMessageType.ServerInfo, regeln.rulesfile.de)
    
end

function regeln.getDescription()
    return "Sendet dir die Regeln des Servers"
end

function regeln.getHelp()
    return "Sendet dir die Regeln des Servers. Usage: /regeln"
end
