package.path = package.path .. ";data/scripts/lib/?.lua"

-- namespace rules
rules = {}
rules.rulesfile = include("\\data\\libs\\rulestrings")

function rules.execute(sender, ...)

	if sender == nil then return end
	Player(sender):sendChatMessage("", ChatMessageType.ServerInfo, rules.rulesfile.en)
    
end

function rules.getDescription()
    return "Sends you the rules of this server."
end

function rules.getHelp()
    return "Sends you the rules of this server. Usage: /rules"
end
