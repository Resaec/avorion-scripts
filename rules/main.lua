package.path = package.path .. ";data/scripts/lib/?.lua"

include ("utility")

-- namespace rulesmain
rulesmain = {}

function rulesmain.getUpdateInterval()
    return 60 * 60 -- tick every 15 minutes
end

function rulesmain.initialize()
	print("==> Rules - loaded")
end

function rulesmain.update(timeStep)
end
