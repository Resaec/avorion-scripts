package.path = package.path .. ";data/scripts/lib/?.lua"

include ("utility")

-- namespace avorionmail
avorionmail = {}

avorionmail.updateMulti = 1/60

function avorionmail.getUpdateInterval()

    return 60 * avorionmail.updateMulti
	
end

function avorionmail.initialize()

	print("==> AvorionMailCheck - loaded")
	
	Server():registerCallback("onPlayerLogIn", "handlePlayerLogIn")
	Galaxy():registerCallback("onPlayerCreated", "handleReturningPlayer")
	
end

function avorionmail.update(timeStep)

	avorionmail.updateMulti = 60
	
end

function avorionmail.handlePlayerLogIn(index)

	local player = Player(index)
	if player == nil then
		print("AMC: Player was nil")
		return
	end
	
	player:registerCallback("onMailAdded", "handlePlayerMailReceived")
	
end

function avorionmail.handlePlayerMailReceived(playerIndex, mailIndex)

	local code = [[
		package.path = package.path .. ";data/scripts/lib/?.lua"
		
		function run(playerIndex, mailIndex)

			local player = Player(playerIndex)
			if player == nil then
				print("AMC: HPMR Player was nil")
				return
			end
			
			local mail = player:getMail(mailIndex)
			if mail == nil then
				print("AMC: HPMR Mail is nil")
				return
			end
			
			-- if the player is cleared, stop here
			if player:getValue("hadAvorion") then return end
			
			-- get player avorion count
			local  _, _, _, _, _, _, playerAvorion = player:getResources()
			
			-- if the player has avorion we are good
			if playerAvorion > 0 then
				
				player:setValue("hadAvorion", true)
				return
				
			end
			
			-- don't delete mails to yourself
			if mail.sender == player.name then return end
			
			local iron, titan, naonit, trinium, xanion, orgonit, avorion = mail:getResources()
			
			-- ignore the mail if it contains no avorion
			if avorion == 0 then return end
			
			-- the player does not have avorion yet, do not allow this mail
			print("AVORION MAIL - '" .. mail.sender .. "' sendete '" .. player.name .. "' " .. avorion .. " Avorion")
			
			avorion = 0
			mail:setResources(iron, titan, naonit, trinium, xanion, orgonit, avorion)
			mail.text = "THIS MAIL CONTAINED AVORION. THIS IS A VIOLATION OF THE RULES AND HAS BEEN LOGGED\n\n" .. mail.text
			
			player:updateMail(mail, mailIndex)
			
			return
		
		end
	]]
	
	async("mailCheckedCallback", code, playerIndex, mailIndex)
	
end

function avorionmail.handleReturningPlayer(playerIndex)
	
	local code = [[
		package.path = package.path .. ";data/scripts/lib/?.lua"
		
		function run(playerIndex)
		
			local player = Player(playerIndex)
			if player == nil then
				print("AMC: HRP Player was nil")
				return
			end
			
			-- if the player is cleared, stop here
			if player:getValue("hadAvorion") then return end
			
			-- get player avorion count
			local  _, _, _, _, _, _, playerAvorion = player:getResources()
			
			-- if the player has avorion we are good
			if playerAvorion > 0 then
			
				player:setValue("hadAvorion", true)
				return
			
			end
			
			-- if the player has no mail, stop here
			if player.numMails == 0 then return	end

			for i = 0, player.numMails - 1, 1
			do
				
				local mail = player:getMail(i)
				if mail == nil then
					print("AMC: HRP Mail is nil")
					return
				end
				
				-- don't delete mails you send to yourself
				if mail.sender == player.name then return end
				
				-- ignore the mail if it is read already
				if not mail.read then
				
					local iron, titan, naonit, trinium, xanion, orgonit, avorion = mail:getResources()
				
					-- ignore the mail if it contains no avorion
					if avorion ~= 0 then
						
						-- the player does not have avorion yet, do not allow this mail
						print("AVORION MAIL - '" .. mail.sender .. "' sendete '" .. player.name .. "' " .. avorion .. " Avorion")
						--print(string.format("AVORION MAIL - '%s' sendete '%s' %d Avorion", mail.sender, player.name, avorion))
						
						avorion = 0
						mail:setResources(iron, titan, naonit, trinium, xanion, orgonit, avorion)
						mail.text = "THIS MAIL CONTAINED AVORION. THIS IS A VIOLATION OF THE RULES AND HAS BEEN LOGGED\n\n" .. mail.text
						--mail.text = string.format("%s\n\n%s", "THIS MAIL CONTAINED AVORION. THIS IS A VIOLATION OF THE RULES AND HAS BEEN LOGGED", mail.text)
						
						player:updateMail(mail, i)
						
					end
					
				end
				
			end
			
			return
		
		end
	]]
	
	async("mailCheckedCallback", code, playerIndex)
	
end

function avorionmail.mailCheckedCallback()
end
