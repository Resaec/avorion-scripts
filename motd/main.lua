package.path = package.path .. ";data/scripts/lib/?.lua"

include ("utility")
message = include("\\data\\libs\\message")

-- namespace motd
motd = {}

function motd.getUpdateInterval()
    return 15 * 60 -- tick every 15 minutes
end

function motd.initialize()
	print("==> MOTD - loaded")
end

function motd.update(timeStep)
    motd.printMotd()
end

function motd.printMotd()

	local server = Server()
	
	if server.players == 0 then return end
    server:broadcastChatMessage("", ChatMessageType.Information, message.motd)
	
end
