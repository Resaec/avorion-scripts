package.path = package.path .. ";data/scripts/lib/?.lua"

message = include("\\data\\libs\\message")

-- namespace motdcommand
motdcommand = {}

function motdcommand.execute(sender, ...)

	if sender == nil then return end
	Player(sender):sendChatMessage("", 3, message.motd)
    
end

function motdcommand.getDescription()
    return "Zeigt die MOTD erneut an / Shows the MOTD"
end

function motdcommand.getHelp()
    return "Zeigt die MOTD erneut an / Shows the MOTD - /motd"
end
